DROP table IF EXISTS trabajador;

create table trabajador(
	id int not null auto_increment primary key,
	nombre varchar(250) null,
	apellido varchar(250) null,
	trabajo ENUM('barrendero', 'oficinista', 'informatico', 'soldador') null,
	salario int null
);

insert into trabajador (nombre, apellido, trabajo, salario)values('Jose','Marin','soldador',30000);
insert into trabajador (nombre, apellido, trabajo, salario)values('Alberto','Calvó','soldador',30000);
insert into trabajador (nombre, apellido, trabajo, salario)values('Oscar','Trillas','oficinista',30000);

