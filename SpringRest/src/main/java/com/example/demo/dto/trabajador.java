package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.*;

@Entity
@Table(name="trabajador")
public class trabajador {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column (name = "nombre")
	private String nombre;
	
	@Column (name = "apellido")
	private String apellido;
	
	@Column (name = "trabajo")
	private String trabajo;
	
	@Column (name = "salario")
	private int salario;
	
	public trabajador() {
		
	}
	
	/**
	 * @param id
	 * @param nombre
	 * @param apellido
	 * @param trabajo
	 */
	//Aqui creo un trabajador en el que no pasas el salario, ya que se asigna solo segun el trabajo
	public trabajador(Long id, String nombre, String apellido, String trabajo) {
		//super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.trabajo = trabajo;
		this.salario = 0;
	}
	
	/**
	 * @param id
	 * @param nombre
	 * @param apellido
	 * @param trabajo
	 * @param salario
	 */
	public trabajador(Long id, String nombre, String apellido, String trabajo, int salario) {
		//super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.trabajo = trabajo;
		this.salario = salario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTrabajo() {
		return trabajo;
	}

	public void setTrabajo(String trabajo) {
		this.trabajo = trabajo;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}

	@Override
	public String toString() {
		return "trabajador [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", trabajo=" + trabajo
				+ ", salario=" + salario + "]";
	}
	
	public static int assignarSalario(String trabajo) {
		int salario = 0;
		
		if(trabajo.contentEquals("barrendero")) {
			salario = 35000;
		}else if(trabajo.contentEquals("oficinista")) {
			salario = 60000;
		}else if(trabajo.contentEquals("informatico")) {
			salario = 80000;
		}else if(trabajo.contentEquals("soldador")) {
			salario = 30000;
		}
		
		return salario;
		
	}
	
	
}
