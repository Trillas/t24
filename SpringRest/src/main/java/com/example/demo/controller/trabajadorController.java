package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.trabajador;
import com.example.demo.service.trabajadorImpl;

@RestController
@RequestMapping("/api")
public class trabajadorController {
	
	@Autowired
	trabajadorImpl trabajadorImpl;
	
	@GetMapping("/Trabajador")
	public List<trabajador> listarTrabajador(){
		return trabajadorImpl.listarTrabajador();
	}
	
	@PostMapping("/Trabajador")
	public trabajador salvarCliente(@RequestBody trabajador trabajador) {
		
		return trabajadorImpl.guardarTrabajador(trabajador);
	}
	
	@GetMapping("/Trabajador/{id}")
	public trabajador trabajadorXId(@PathVariable(name = "id") Long id) {
		
		trabajador trabajador_xid = new trabajador();
		
		trabajador_xid = trabajadorImpl.trabajadorXID(id);
		
		System.out.println("Trabajador por ID: " + trabajador_xid);
		
		return trabajador_xid;
	}
	
	@GetMapping("/Trabajador/trabajos/{trabajo}")
	public List<trabajador> trabajadorXTrabajo(@PathVariable(name = "trabajo") String trabajo) {
		return trabajadorImpl.trabajadorXTrabajo(trabajo);
	}
	
	@PutMapping("/Trabajador/{id}")
	public trabajador actualizarTrabajador(@PathVariable(name="id")Long id,@RequestBody trabajador trabajador) {
		
		trabajador trabajadorSeleccionado = new trabajador();
		trabajador trabajadorActualizado = new trabajador();
		
		trabajadorSeleccionado = trabajadorImpl.trabajadorXID(id);
		
		trabajadorSeleccionado.setNombre(trabajador.getNombre());
		trabajadorSeleccionado.setApellido(trabajador.getApellido());
		trabajadorSeleccionado.setTrabajo(trabajador.getTrabajo());
		trabajadorSeleccionado.setSalario(com.example.demo.dto.trabajador.assignarSalario(trabajador.getTrabajo()));
		
		trabajadorActualizado = trabajadorImpl.actualizarTrabajador(trabajadorSeleccionado);
		
		System.out.println("El que se acaba de actualizar es: " + trabajadorActualizado);
		
		return trabajadorActualizado;
		
	}
	
	@DeleteMapping("/Trabajador/{id}")
	public void eliminarTrabajador(@PathVariable(name="id")Long id) {
		trabajadorImpl.eliminarTrabajador(id);
	}
	
	
}
