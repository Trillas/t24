package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.trabajador;

public interface trabajadorDao extends JpaRepository<trabajador, Long> {

	public List<trabajador> findByTrabajo(String trabajo);
	
}

