package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.trabajadorDao;
import com.example.demo.dto.trabajador;

@Service
public class trabajadorImpl {
	
	@Autowired
	trabajadorDao trabajadorDao;

	public List<trabajador> listarTrabajador() {
		return trabajadorDao.findAll();
	}
	
	public trabajador guardarTrabajador(trabajador trabajador) {
		trabajador.setSalario(com.example.demo.dto.trabajador.assignarSalario(trabajador.getTrabajo()));
		return trabajadorDao.save(trabajador);
	}

	public trabajador trabajadorXID(Long id) {
		return trabajadorDao.findById(id).get();
	}
	
	public List<trabajador> trabajadorXTrabajo(String trabajo) {
		
		return trabajadorDao.findByTrabajo(trabajo);
	}

	public trabajador actualizarTrabajador(trabajador trabajador) {
		
		return trabajadorDao.save(trabajador);
	}

	public void eliminarTrabajador(Long id) {
		
		trabajadorDao.deleteById(id);
		
	}
}
