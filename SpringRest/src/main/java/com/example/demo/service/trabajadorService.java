package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.trabajador;

public interface trabajadorService {
	
	public List<trabajador> listarTrabajador();
	
	public trabajador guardarTrabajador(trabajador trabajador);
	
	public trabajador trabajadorXId(Long id);
	
	public trabajador actualizarTrabajador(trabajador trabajador);
	
	public void eliminarTrabajador(Long id);
}
